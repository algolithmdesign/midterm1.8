/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.algo;

import java.util.*;

/**
 *
 * @author AdMiN
 */
public class Mid_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] Array = new int [n];
        for (int i = 0; i < n; i++) {
            Array[i] = in.nextInt();
        }
        System.out.println( Arrays.toString(Array));
        reverseArray(Array, Array.length);
        System.out.print(Arrays.toString(Array));
    }

    static void reverseArray(int Array[], int length) {
        for (int i = 0; i < length / 2; i++) {
            int count = Array[i];
            Array[i] = Array[length - i - 1];
            Array[length - i - 1] = count;
        }
    }
}
